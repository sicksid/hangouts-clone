root = "#{Dir.getwd}"

activate_control_app "tcp://127.0.0.1:9292"
bind "unix://#{root}/../shared/puma.sock"
pidfile "#{root}/../shared/pids/puma.pid"
rackup "#{root}/config.ru"
state_path "#{root}/../shared/pids/puma.state"
daemonize true