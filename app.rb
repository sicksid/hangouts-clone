require 'sinatra'
require 'sinatra/cookies'
require 'sinatra/custom_logger'
require 'http'
require 'json'
require 'logger'
require 'pry-byebug'
require './models'

CABLE_URL = '/cable'

class App < Sinatra::Application  
  helpers Sinatra::CustomLogger

  configure :development, :production do
    log_path = "#{root}/log/#{environment}.log"
    unless File.exist?(log_path)
      File.new log_path, 'w+'
    end
    logger = Logger.new(File.open(log_path, 'a'))
    logger.level = Logger::DEBUG if development?
    set :logger, logger
  end

  enable :sessions
  enable :logging

  set :haml, :format => :html5
  set :public_folder, 'static'
  set :views, 'templates'
  set :port, ENV["PORT"]

  get '/' do
    @calls = Call.all
    haml :index
  end

  post '/call/create' do
    title = params[:title]
    pincode = params[:pincode]
    description = params[:description]

    @call = Call.create(
      :title => title.empty? ? nil : title,
      :pincode => pincode.empty? ? nil : pincode,
      :description => description.empty? ? nil : description,
    )
    
    haml :info
  end

  get '/call/:slug' do
    unless @call = Call.first(:slug => params["slug"])
      return redirect "/"
    end
    if (not @call.pincode.nil? and not session[:authenticated]) or session[:nickname].nil?
      return redirect "/call/#{@call.slug}/authenticate"
    end

    @nickname = session[:nickname]
    @ice_servers = HTTP.basic_auth(
      :user => ENV['XIRSYS_USER'], :pass => ENV['XIRSYS_PASS']
    ).put(
      "https://global.xirsys.net/_turn/hangouts-clone/"
    )

    haml :call
  end

  get '/call/:slug/authenticate' do
    unless @call = Call.first(:slug => params["slug"])
      redirect '/'
    end
    haml :authenticate
  end

  post '/call/:slug/authenticate' do
    unless @call = Call.first(:slug => params["slug"])
      redirect '/'
    end

    unless @call.pincode.nil?
      if @call.pincode == params["pincode"]
        session[:authenticated] = true
      else
        @error = "the pincode entered for this call is not correct"
        return haml :authenticate
      end
    end
    
    session[:nickname] = params['nickname']
    cookies[:nickname] = params['nickname']
    redirect "/call/#{@call.slug}"
  end
end
