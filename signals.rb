require "litecable"
require 'pry-byebug'

UsersList = Hash.new

module Signals
  class Connection < LiteCable::Connection::Base
    identified_by :nickname, :sid

    def connect
      @nickname = cookies["nickname"]
      @sid = request.params["sid"]
    end

    def disconnect
      $stdout.puts "#{@nickname} disconnected"
    end
  end

  class Channel < LiteCable::Channel::Base
    identifier :chat

    def subscribed
      reject unless chat_id
      if UsersList[chat_id].nil?
        UsersList[chat_id] = Array.new
      end
      UsersList[chat_id].push(nickname)
      $stdout.puts UsersList
      stream_from "chat_#{chat_id}"
      stream_from "chat_#{chat_id}_#{nickname}"
      LiteCable.broadcast "chat_#{chat_id}", type: "userslist", list: UsersList[chat_id]
    end

    def unsubscribed
      index = UsersList[chat_id].index(nickname)
      if index
        UsersList[chat_id].delete_at(index)
      end
      
      $stdout.puts UsersList
      LiteCable.broadcast "chat_#{chat_id}", type: "userslist", list: UsersList[chat_id]
    end

    def speak(data)
      LiteCable.broadcast "chat_#{chat_id}", nickname: nickname, message: data["message"], sid: sid, type: "message"
    end

    def signaling(data)
      $stdout.puts data
      LiteCable.broadcast "chat_#{chat_id}_#{data['target']}", type: "signaling", message: data, sid: sid, nickname: nickname
    end

    private

    def chat_id
      params.fetch("id")
    end
  end
end