
class PeerConnection {
    constructor(props) {
        const peer = new RTCPeerConnection({
            iceServers: iceServers
        })
        peer.onicecandidate = this.iCECandidateEvent.bind(this)
        peer.onnremovestream = this.removeStreamEvent.bind(this)
        peer.oniceconnectionstatechange = this.iCEConnectionStateChangeEvent.bind(this)
        peer.onsignalingstatechange = this.signalingStateChangeEvent.bind(this)
        peer.onnegotiationneeded = this.negotiationNeededEvent.bind(this)
        peer.ontrack = this.trackEvent.bind(this)
        this.peer = peer
        this.localVideo = props.localVideo
        this.remoteVideo = props.remoteVideo
    }

    iCECandidateEvent(event) {
        if (!event.candidate) {
            return
        }
        chatChannel.perform('signaling', {
            type: "new-ice-candidate",
            target: targetNickname,
            nickname: nickname,
            candidate: event.candidate
        })
    }

    removeStreamEvent() {
        this.close()
    }

    iCEConnectionStateChangeEvent(event) {
        if (this.peer.iceConnectionState == 'disconnected') {
            this.close()
        }
    }

    signalingStateChangeEvent() {
        if (this.peer.signalingState == 'closed') {
            this.close()
        }

    }

    createOffer(callback) {
        this.peer.createOffer(offerOptions).then(
            offer => this.peer.setLocalDescription(offer)
        ).then(() => {
            chatChannel.perform('signaling', {
                nickname: nickname,
                target: targetNickname,
                type: "video-offer",
                sdp: this.peer.localDescription
            })
            if (callback) {
                callback()
            }
        })
    }

    negotiationNeededEvent() {
        this.createOffer()
    }
    trackEvent(event) {
        this.remoteVideo.srcObject = event.streams[0]
    }

    addIceCandidate(candidate) {
        this.peer.addIceCandidate(candidate).catch(error => console.log(error.name, error.message))
    }

    close() {
        if (!this.peer) {
            return
        }
        this.peer.onicecandidate = null
        this.peer.onnremovestream = null
        this.peer.oniceconnectionstatechange = null
        this.peer.onicegatheringstatechange = null
        this.peer.onsignalingstatechange = null
        this.peer.onnegotiationneeded = null
        this.peer.ontrack = null

        if (this.localVideo.srcObject) {
            this.localVideo.srcObject.getTracks().forEach(
                track => track.stop()
            )
        }

        if (this.remoteVideo.srcObject) {
            this.remoteVideo.srcObject.getTracks().forEach(
                track => track.stop()
            )
        }

        this.localVideo = null
        this.peer.close()
        this.peer = null
    }

}

let $chat = document.getElementById('chat')
let $textarea = document.getElementsByTagName('textarea')[0]
let $send = document.getElementById('send')
let $localVideo = document.getElementById('local')
let $remoteVideo = document.getElementById('remote')
let $call = document.getElementById('call')
let $join = document.getElementById('join')
let $hangup = document.getElementById('hangup')
let localPeerConnection = null
let remotePeerConnection = null
let targetNickname = null

document.getElementsByTagName('form')[0].addEventListener('submit', function (event) {
    event.preventDefault()
})

$send.addEventListener('click', function (event) {
    const message = $textarea.value
    if (!message) {
        return
    }
    chatChannel.perform('speak', {
        message: message
    })
})

$textarea.addEventListener('keypress', function(event) {
    const key = event.which || event.keyCode
    if (key == 13) {
        const message = $textarea.value
        if (!message) {
            $textarea.value = null
            return
        }
        chatChannel.perform('speak', {
            message: message
        })
    }
})

function addMessage(data) {
    const timestamp = new Date().toLocaleTimeString();
    const isLocal = nickname == data.nickname
    const div = document.createElement('div')
    div.innerHTML = `<span class="nickname ${isLocal ? 'local' : 'remote'}">${data.nickname}</span> ${timestamp}: ${data.message}`
    div.classList.add('message')
    $chat.appendChild(div)
    div.scrollIntoView()
    $textarea.value = ''
}

ActionCable.startDebugging()
let socketId = Date.now()
let cable = ActionCable.createConsumer('/cable' + '?sid=' + socketId)
let chatChannel = cable.subscriptions.create({
    channel: 'chat',
    id: roomId
}, {
    received: function (data) {
        if (data.type == 'signaling') {
            data = data.message
        }
        switch (data.type) {
            case 'message':
                addMessage(data)
            break

            case 'userslist':
                if (data.list.length > 1) {
                    $call.disabled = false
                }

                if (data.list.length == 1) {
                    $call.disabled = true
                }
                targetNickname = data.list.find(user => user != nickname)
                if (targetNickname) {
                    $remoteVideo.poster = `https://via.placeholder.com/640x480?text=${targetNickname}`
                }
            break

            case 'video-offer':
                if (localPeerConnection == null) {
                    localPeerConnection = new PeerConnection({
                        localVideo: $localVideo,
                        remoteVideo: $remoteVideo,
                    })
                    let description = new RTCSessionDescription(data.sdp)
                    localPeerConnection.peer.setRemoteDescription(description).then(
                        () => navigator.mediaDevices.getUserMedia({
                            audio: true,
                            video: true
                        })
                    ).then(stream => {
                        $localVideo.srcObject = stream
                        stream.getTracks().forEach(
                            track => localPeerConnection.peer.addTrack(track, stream)
                        )
                        $hangup.disabled = false
                    }).then(() => localPeerConnection.peer.createAnswer()).then(
                        answer => localPeerConnection.peer.setLocalDescription(answer)
                    ).then(() => chatChannel.perform('signaling', {
                        nickname,
                        target: targetNickname,
                        type: 'video-answer',
                        sdp: localPeerConnection.peer.localDescription
                    }))
                }
            break
            case 'video-answer':
                let description = new RTCSessionDescription(data.sdp)
                localPeerConnection.peer.setRemoteDescription(description).then(() => $hangup.disabled = false).catch(error => console.log(error))
            break

            case 'new-ice-candidate':
                const candidate = new RTCIceCandidate(data.candidate)
                localPeerConnection.peer.addIceCandidate(candidate)
            break
            case 'hang-up':
                hangup()
            break

        }
    }
})

function startNavigatorUserMedia() {
    navigator.mediaDevices
        .getUserMedia({
            audio: true,
            video: true
        })
        .then(function (stream) {
            $localVideo.srcObject = stream
        })
        .catch(e => alert(`getUserMedia() error: ${e.name}`))
}

startNavigatorUserMedia()

const offerOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
}

$call.onclick = function call() {
    localPeerConnection = new PeerConnection({
        localVideo: $localVideo,
        remoteVideo: $remoteVideo
    })
    let stream = $localVideo.srcObject
    stream.getTracks().forEach(track => localPeerConnection.peer.addTrack(track, stream))
}

function hangup() {
    if (localPeerConnection) {
        chatChannel.perform('signaling', {
            nickname: nickname,
            target: targetNickname,
            type: 'hang-up',
        })
        localPeerConnection.close()
        localPeerConnection = null
        $remoteVideo.srcObject = null
        $localVideo.srcObject = null
        $hangup.disabled = true

        startNavigatorUserMedia() 

    }
}

$hangup.onclick = hangup