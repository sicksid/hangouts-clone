require 'rack/test'
require 'rspec'
require 'sinatra/base'

require File.join(File.dirname(__FILE__), '..', 'factories.rb')
require File.join(File.dirname(__FILE__), '..', 'app.rb')

ENV['RACK_ENV'] = 'test'
TEST_DATABASE_PATH = File.join(File.dirname(__FILE__), '..', 'test_database.db')
if File.exists? TEST_DATABASE_PATH
  File.delete(TEST_DATABASE_PATH)
end

RSpec.configure do |configure|
    configure.include Rack::Test::Methods
    def app() App end
    DataMapper.setup(:default, "sqlite3://#{TEST_DATABASE_PATH}")
    DataMapper.auto_upgrade!
end


