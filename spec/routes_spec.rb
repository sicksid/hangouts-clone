require 'spec_helper'

describe "Hangouts Clone App" do
  it "should allow accessing the home page" do
    get '/'
    expect(last_response).to be_ok
  end

  it "should allow you to create a call without a pincode" do
    post "/call/create", {:title => "example", :description => "lorem ipsum ble ble"}
    expect(last_response).to be_ok
  end

  it "should redirect you to authenticate before letting you enter the call" do
    call = Factory.create_call()
    call.pincode = nil
    call.save!
    get "/call/#{call.slug}"
    post "/call/#{call.slug}/authenticate", {:nickname => 'test'}
    expect(last_response).to be_redirect
    follow_redirect!
    expect(last_response).to be_ok
  end


  it "should redirect to authenticate if a call has a pincode" do
    call = Factory.create_call()
    get "/call/#{call.slug}"
    expect(last_response).to be_redirect
    follow_redirect!
    post "/call/#{call.slug}/authenticate", {:nickname => 'test', :pincode => call.pincode}
    expect(last_response).to be_redirect
    follow_redirect!
    expect(last_response).to be_ok
  end
end
