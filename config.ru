require 'dotenv/load'
require 'rubygems'
require 'bundler'
require 'lite_cable'
require 'lite_cable/server'
require 'rerun'

Bundler.require

require './app'
require './signals'


app = Rack::Builder.new do
  map '/' do
      run App
  end
end

if ENV['ANYCABLE']
  LiteCable.anycable!
else
  require "lite_cable/server"

  app.map '/cable' do
    use LiteCable::Server::Middleware, connection_class: Signals::Connection
    run proc { |_| [200, { 'Content-Type' => 'text/plain' }, ['OK']] }
  end
end

run app