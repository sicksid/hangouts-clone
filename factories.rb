require 'faker'
require './models'

module Factory
    def Factory.create_call(title=nil, pincode=nil)
        unless title
            title = Faker::Movie.quote
        end

        unless pincode
            pincode = Faker::Number.number(8)
        end

        Call.create(
            :title => title,
            :pincode => pincode,
            :description => Faker::Lorem.paragraph(2, true)
        )
    end

    def Factory.create_participant(call=nil)
        unless call
            call = Factory.create_call
        end

        Participant.create(
            :call => call,
            :email => Faker::Internet.email,
            :nickname => Faker::Internet.username
        )
    end
end

