require 'data_mapper'
DataMapper.setup(:default, "sqlite://#{Dir.pwd}/database.db")
class Call
  include DataMapper::Resource
  property :id, Serial
  property :description, String
  property :pincode, String
  property :title, String, :required => true, :unique => true
  property :slug, String, :unique => true

  before :save do |call|
    call.slug = call.title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end
end

class Participant
  include DataMapper::Resource

  property :id, Serial
  property :email, String
  property :nickname, String

  belongs_to :call
end

DataMapper.auto_upgrade!